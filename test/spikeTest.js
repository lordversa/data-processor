import http from "k6/http";

import { check, sleep } from "k6";

export const options = {
  stages: [
    { duration: "10s", target: 50 },
    { duration: "10s", target: 0 },
  ],
  thresholds: {
    http_req_duration: ["p(99) < 400"],
  },
};

export default () => {
  const response = http.get(`http://localhost:3000/`);
  sleep(1);
  check(response, {
    "Every request status is 200": (r) => {
      return [200].includes(r.status);
    },
  });
};
