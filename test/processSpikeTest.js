import http from "k6/http";

import { check, sleep } from "k6";

export const options = {
  stages: [
    { duration: "10s", target: 50 },
    { duration: "10s", target: 0 },
  ],
  thresholds: {
    http_req_duration: ["p(99) < 400"],
  },
};

export default () => {
  const response = http.post(`http://localhost:3000/api/process/`, {
    user_id: 1,
    token: "tokaggwgwaen",
  });
  sleep(1);
  check(response, {
    "Every request statuses are 200 or 429": (r) => {
      return [200, 429].includes(r.status);
    },
  });
};
