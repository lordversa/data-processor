import * as Sentry from "@sentry/node";
import { ProfilingIntegration } from "@sentry/profiling-node";
import { Application } from "express";
import config from "../Config/index.js";

export class ExpressSentry {
  static async initialize(app: Application) {
    Sentry.init({
      dsn: config.sentryDsn,
      integrations: [
        new Sentry.Integrations.Http({ tracing: true }),
        new Sentry.Integrations.Express({ app }),
        new ProfilingIntegration(),
      ],
      tracesSampleRate: config.sentryTraceSampleRate,
      profilesSampleRate: config.sentryProfileSampleRate,
    });
    app.use(Sentry.Handlers.requestHandler());

    app.use(Sentry.Handlers.tracingHandler());
  }

  static async afterRoutesInitialize(app: Application) {
    app.use(Sentry.Handlers.errorHandler());

    app.use(function onError(err: any, req: any, res: any, next: any) {
      return res.status(500).send("Something went wrong, Please try later.");
    });
  }
}
