import { Redis } from "ioredis";
import config from "../Config/index.js";
const redisClient = new Redis({
  enableOfflineQueue: false,
  host: config.redis.host,
  port: config.redis.port,
  db: config.redis.db,
});

import { RateLimiterRedis } from "rate-limiter-flexible";

redisClient.on("error", (err) => {});

export const consume = (userId: number, points: number, duration: number) => {
  const opts = {
    storeClient: redisClient,
    points,
    duration,
    blockDuration: 0,
    keyPrefix: "rlflx",
  };

  const rateLimiterRedis = new RateLimiterRedis(opts);
  return rateLimiterRedis
    .consume(`UserId:${userId}:Duration:${duration}`)
    .then((rateLimiterRes) => {
      return {
        finished: false,
        remains: rateLimiterRes.remainingPoints,
        remainTime: Math.round(rateLimiterRes.msBeforeNext / 1000) || 1,
      };
    })
    .catch((rejRes) => {
      if (!(rejRes instanceof Error)) {
        return {
          finished: true,
          remains: rejRes.remainingPoints,
          remainTime: Math.round(rejRes.msBeforeNext / 1000) || 1,
        };
      }
    });
};
