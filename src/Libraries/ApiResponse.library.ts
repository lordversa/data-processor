import { Request, Response } from "express";

export class ApiResponse {
  static NotFoundResponse(req: Request, res: Response) {
    return res.status(404).json({
      url: req.originalUrl,
      message: "Url Not Found",
    });
  }
}
