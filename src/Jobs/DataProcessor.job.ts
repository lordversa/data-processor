import amqp from "amqplib";
import * as DataProcessorInterface from "../Interfaces/DataProcessor.interface.js";
import RabbitMQConnection from "./Rabbitmq.js";
import * as Sentry from "@sentry/node";
const queueName = "data_processor";

export class DataProcessorJob {
  private static channel: amqp.Channel | null = null;

  private static async getChannel(): Promise<amqp.Channel> {
    try {
      if (!this.channel) {
        this.channel = await (await RabbitMQConnection.getConnection()).createChannel();
        await this.channel.assertQueue(queueName, { durable: true });
      }
    } catch (error) {
      console.error(error);
      Sentry.captureException(error, {
        extra: {
          additionalInfo: "Error on creating rabbitmq channel.",
        },
      });
    }
    return this.channel;
  }

  static async dispatch(event: DataProcessorInterface.Event) {
    try {
      const channel = await this.getChannel();
      return channel.sendToQueue(queueName, Buffer.from(JSON.stringify(event)));
    } catch (error) {
      console.error(error);
      Sentry.captureException(error, {
        extra: {
          additionalInfo: "Error on sending event to queue.",
        },
      });
    }
  }
}
