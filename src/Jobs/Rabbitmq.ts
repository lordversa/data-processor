import * as amqp from "amqplib";
import * as Sentry from "@sentry/node";

class RabbitMQConnection {
  private static connection: amqp.Connection | null = null;

  static async getConnection(): Promise<amqp.Connection> {
    try {
      if (!this.connection) {
        this.connection = await amqp.connect("amqp://localhost");

        process.once("SIGINT", async () => {
          await this.connection.close();
        });
      }
      return this.connection;
    } catch (error) {
      console.error(error);
      Sentry.captureException(error, {
        extra: {
          additionalInfo: "Error on creating rabbitmq connection.",
        },
      });
    }
  }
}

export default RabbitMQConnection;
