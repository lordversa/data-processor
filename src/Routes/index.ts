import { Application } from "express";

import DataProcessorController from "../Controllers/DataProcess.controller.js";
import { processDataValidator as processDataValidatorMiddleware } from "../Middlewares/ProcessDataValidator.middleware.js";
import { rateLimiter as rateLimiterMiddleware } from "../Middlewares/RateLimiter.middleware.js";
import { userValidation as userValidationMiddleware } from "../Middlewares/UserValidation.middleware.js";

export const RegisterAppRoutes = (app: Application) => {
  /**
   * @swagger
   * /api/process:
   *   post:
   *     summary: Process user data
   *     description: Endpoint to process user data.
   *     tags: [Users]
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user_id
   *         description: User's id.
   *         in: formData
   *         required: true
   *         type: string
   *       - name: token
   *         description: Data's token.
   *         in: formData
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: Successful processing.
   *       400:
   *         description: Bad request. (Include additional error responses as needed)
   *       401:
   *         description: Unauthorized. (Include additional error responses as needed)
   *       429:
   *         description: Too many requests. Rate limit exceeded.
   *       500:
   *         description: Internal server error. (Include additional error responses as needed)
   */



  app.use(
    "/api/process",
    processDataValidatorMiddleware,
    userValidationMiddleware,
    rateLimiterMiddleware,
    DataProcessorController
  );
  app.get("/", (req, res) => {
    res.send("ok");
  });
};
