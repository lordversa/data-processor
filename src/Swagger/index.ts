const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Data Processor",
      version: "1.0.0",
      description: "Interview Data Processor",
    },
  },
  apis: ["./build/Routes/index.js"],
};

import { Application } from "express";
import swaggerJSDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";
const swaggerSpec = swaggerJSDoc(swaggerOptions);
export const initialSwagger = (app: Application) => {
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));
};
