import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
  OneToMany,
} from "typeorm";
import { UserQuota } from "./UserQuota.js";
import { AppDataSource } from "../Database/index.js";

@Entity({
  name: "users",
})
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany("UserQuota", "user", { eager: true })
  quotas: UserQuota[];

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  static async findById(id: number) {
    const userRepository = AppDataSource.getRepository(User);
    return await userRepository.findOneBy({ id });
  }
}
