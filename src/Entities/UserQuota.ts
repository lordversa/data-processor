import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
  ManyToOne,
} from "typeorm";
import { User } from "./User.js";

export enum QuotaTypes {
  Hourly = "hourly",
  Monthly = "monthly",
}

@Entity({
  name: "user-quotas",
})
export class UserQuota {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne("User", "quotas")
  user: User;

  @Column({ type: "varchar" })
  type: QuotaTypes;

  @Column({ type: "bigint" })
  value: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
