import * as dotenv from "dotenv";
import moment from "moment";

dotenv.config();

const config = {
  env: process.env.NODE_ENV || "development",
  port: process.env.PORT || 3000,
  sentryDsn:
    process.env.SENTRY_DSN ,
  sentryTraceSampleRate: parseFloat(process.env.SENTRY_TRACE_SAMPLE_RATE) || 0.1,
  sentryProfileSampleRate: parseFloat(process.env.SENTRY_PROFILE_SAMPLE_RATE) || 0.1,
  amqpUrl: process.env.AMQP_URL || "amqp://localhost",
  mysql: {
    host: process.env.MYSQL_HOST || "localhost",
    port: +process.env.MYSQL_PORT || 3306,
    username: process.env.MYSQL_USERNAME || "root",
    password: process.env.MYSQL_PASSWORD || "1",
    database: process.env.MYSQL_DATABASE || "data-processor",
  },
  sessionSecret: process.env.SESSION_SECRET || "secret",
  redis: {
    host: process.env.REDIS_HOST || "localhost",
    port: +process.env.REDIS_PORT || 6379,
    db: +process.env.REDIS_DB || 0,
  },
};

export default config;
