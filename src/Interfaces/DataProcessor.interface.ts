export interface Event {
  userId: string;
  token: string;
}
