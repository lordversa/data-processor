import express from "express";
import cors from "cors";
import "reflect-metadata";

import config from "./Config/index.js";
import { ApiResponse } from "./Libraries/ApiResponse.library.js";
import { RegisterAppRoutes } from "./Routes/index.js";
import bodyParser from "body-parser";
import { connectDatabase } from "./Database/index.js";
import session from "express-session";
import { User } from "./Entities/User.js";
import { initialSwagger } from "./Swagger/index.js";

connectDatabase();

const app = express();
app.use(express.json());

app.use(
  cors({
    origin: "*",
  })
);

declare module "express-session" {
  interface SessionData {
    user: User;
  }
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: config.sessionSecret, resave: false, saveUninitialized: false }));

// ExpressSentry.initialize(app);

RegisterAppRoutes(app);
initialSwagger(app);

app.get("*", (req, res) => {
  return ApiResponse.NotFoundResponse(req, res);
});

// ExpressSentry.afterRoutesInitialize(app);
app.listen(config.port, () => {
  console.log(`Server Started On Port:${config.port}`);
});
