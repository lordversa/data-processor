import * as express from "express";
import * as DataProcessValidations from "../Validations/DataProcess.validation.js";

export const processDataValidator = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const errors = DataProcessValidations.process(req.body);
  if (errors.length > 0) {
    return res.status(400).json(errors);
  }

  return next();
};
