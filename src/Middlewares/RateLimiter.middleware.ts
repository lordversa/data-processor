import { log } from "console";
import * as express from "express";
import _ from "lodash";
import { QuotaTypes } from "../Entities/UserQuota.js";
import * as RedisRateLimiter from "../Libraries/RedisRateLimiter.library.js";

export const rateLimiter = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  if (req.session.user.quotas) {
    const hourly = _.find(req.session.user.quotas, { type: QuotaTypes.Hourly });
    if (hourly) {
      const data = await RedisRateLimiter.consume(req.session.user.id, hourly.value, 60);
      if (data.finished) {
        return res
          .header("Retry-After", `${data.remainTime}`)
          .status(429)
          .send("Too Many Requests");
      }
    }
    const monthly = _.find(req.session.user.quotas, { type: QuotaTypes.Monthly });
    if (monthly) {
      const data = await RedisRateLimiter.consume(req.session.user.id, monthly.value, 2592000);
      if (data.finished) {
        return res
          .header("Retry-After", `${data.remainTime}`)
          .status(429)
          .send("Too Many Requests");
      }
    }
  }
  return next();
};
