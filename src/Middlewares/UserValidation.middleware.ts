import * as express from "express";
import { User } from "../Entities/User.js";

export const userValidation = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const user = await User.findById(req.body.user_id);
  if (!user) {
    return res.status(404).json({
      message: "User not found.",
    });
  }
  req.session.user = user;
  return next();
};
