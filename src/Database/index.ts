import { DataSource } from "typeorm";
import config from "../Config/index.js";
export const AppDataSource = new DataSource({
  type: "mysql",
  host: config.mysql.host,
  port: config.mysql.port,
  username: config.mysql.username,
  password: config.mysql.password,
  database: config.mysql.database,
  synchronize: true,
  logging: false,
  entities: ["build/Entities/*.js"],
});

export const connectDatabase = async () => {
  console.log("connecting to database.");

  AppDataSource.initialize()
    .then(() => {
      console.log("Data Source has been initialized!");
      return AppDataSource;
    })
    .catch((err) => {
      console.error("Error during Data Source initialization:", err);
    });
};
