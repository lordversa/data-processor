import * as express from "express";
import { DataProcessorJob } from "../Jobs/DataProcessor.job.js";

const router = express.Router();

router.post("/", async (req: express.Request, res: express.Response) => {
  const { user_id: userId, token } = req.body;
  const response = await DataProcessorJob.dispatch({ userId, token });
  return res.status(200).json({
    response,
  });
});

export default router;
