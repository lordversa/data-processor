export const process = (params: any) => {
  const errors = [];

  if (!params.user_id) {
    errors.push({
      field: "user_id",
      message: "User ID must not be empty",
    });
  }

  if (!params.token) {
    errors.push({
      field: "token",
      message: "Token must not be empty",
    });
  }
  return errors;
};
