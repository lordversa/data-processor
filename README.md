# Data Processor

Welcome to the Data Processor project! This application is designed to efficiently process user data by receiving a user ID and token, sending it to a RabbitMQ queue. Additionally, the project includes a robust rate limiter system that supports various limiter counts for different users, utilizing Redis for efficient management.

### Prerequisites

Before you can run the code, make sure you have the following dependencies installed on your system:

```
Redis
RabbitMQ
MySQL
Node.js (version 20)
```

### Installation

To set up the project, follow these steps:

Install project dependencies using Yarn:

```bash
yarn install
```

Compile TypeScript code:

```bash
tsc
```

Start the application:

```bash
yarn start
```

### Configuration

Make sure to configure your environment variables by creating a .env file in the project root and filling it with the required values. Here's a list of supported configuration variables:

```env
NODE_ENV=production
PORT=3000
SENTRY_DSN=
SENTRY_TRACE_SAMPLE_RATE=0.1
SENTRY_PROFILE_SAMPLE_RATE=0.1
AMQP_URL="amqp://localhost"
MYSQL_HOST="localhost"
MYSQL_PORT=3306
MYSQL_USERNAME="root"
MYSQL_PASSWORD="1"
MYSQL_DATABASE="data-processor"
SESSION_SECRET="secret"
REDIS_HOST="localhost"
REDIS_PORT=6379
REDIS_DB=0
```

Ensure that you provide valid values for each configuration variable to enable seamless operation of the Data Processor.

Running the Application
Once the installation and configuration are complete, your Data Processor application should be up and running. It will be ready to efficiently process user data, handle rate limiting, and interact with the specified RabbitMQ queue.

Feel free to reach out if you have any questions or encounter issues. Happy coding! 🚀
